// Gameboard variables
var MAX_WIDTH_COL = 20,
    MAX_HEIGHT_COL = 20,
    SQUARE_WIDTH = 30,
    MAX_WIDTH = 570,
    HIGHSCORE_ROWS = 10;

// Timeout generators
var MINEGENERATOR = 0,
    GAMETIMEOUR = 0;

// Game data
var NUM_EATEN_APPLES = 0,
    BOARD_STATE = 0, // 0-start, 1-plays, 2-loses
    SNAKE_LENGTH = 1,
    SNAKE_SPEED = 300,
    SNAKE_MOVEMENT = [],
    CUR_DIR,
    DIR_DICT = {0 : [-30, 0], 1: [0, -30], 2: [30, 0], 3: [0, 30]},
    GAMEBOARD_OCCUPIED = [];

//----------------------------------------------------------------
// TODO global, browsers

$(document).ready(function() {
    initialiseGameData();
    initialiseStartContainer();
    generateLocalHighscore();
    initialiseGlobalHighscore();

    $("#btn-start").on("click", function() {
        $(".container-start").css("display", "none");
        startMineGenerator();
        startGame()
    });
});

// GAMEBOARD_OCCUPIED METHODS

/**
 * Method to add elements on gameboard matrix (technically, its a list). Before adding element the method checks if the
 * square is not occupied and if so, it adds an element.
 * @method appendGameboard
 * @param obj - an array with coordinates x and y
 */
function appendGameboard(obj) {
    if (!isOnGameboard(obj)) {
        GAMEBOARD_OCCUPIED.push(obj);
    }
}

/**
 * Method to remove an element from gameboard matrix.
 * @method removeFromGameboard
 * @param obj - an array with coordinates x and y
 */
function removeFromGameboard(obj) {
    for (var i = 0; i < GAMEBOARD_OCCUPIED.length; i++) {
        if (obj[0] === GAMEBOARD_OCCUPIED[i][0] && obj[1] === GAMEBOARD_OCCUPIED[i][1]) {
            GAMEBOARD_OCCUPIED.splice(i, 1);
            return;
        }
    }
}

/**
 * Method to determine if object is on gameboard
 * @method isOnGameboard
 * @param obj - an array with coordinates x and y
 * @return boolean value if object is on gameboard
 */
function isOnGameboard(obj) {
    for (var i = 0; i < GAMEBOARD_OCCUPIED.length; i++) {
        if (obj[0] === GAMEBOARD_OCCUPIED[i][0] && obj[1] === GAMEBOARD_OCCUPIED[i][1]) {
            return true
        }
    }
    return false;
}

// GAME METHODS

/**
 * Method to initialise board state and score, add EventListener, generate random positions for the snake and
 * an apple. Then, put add them to GAMEBOARD_OCCUPIED - its necessary to prevent from generating objects on each
 * other - and put them on the gameboard. Also, the direction is randomized.
 * @method initialiseGameData
 */
function initialiseGameData() {
    BOARD_STATE = 0;
    $("#score").html("Score: 0");
    let $gameBoard = $("#game");
    $gameBoard.empty();
    document.addEventListener("keydown", handleKeyPress);

    do {
        var snake_xpos = randomizePos(MAX_WIDTH_COL),
            snake_ypos = randomizePos(MAX_HEIGHT_COL),
            apple_xpos = randomizePos(MAX_WIDTH_COL),
            apple_ypos = randomizePos(MAX_HEIGHT_COL);
    } while (snake_xpos === apple_xpos || snake_ypos === apple_ypos);

    appendGameboard([snake_xpos, snake_ypos]);
    appendGameboard([apple_xpos, apple_ypos]);

    initObject(snake_xpos, snake_ypos, "apple", "red", $gameBoard);
    initObject(apple_xpos, apple_ypos, "snake-head", "green", $gameBoard);

    CUR_DIR = randomizeSnakeDirection();
}

/**
 * Method used to handle key press. Arrows only allowed. If snake's length is greater than 1, he won't be able to go
 * into opposite direction, e.g. he goes right, has a tail and wants to go back (left) - normally he would die,
 * because he bit himself.
 * @method handleKeyPress
 */
function handleKeyPress() {
    window.onkeyup = function (e) {
        let k = e.keyCode ? e.keyCode : e.which;

        if (k === 37 && ((CUR_DIR !== 0 && SNAKE_LENGTH === 1) || (SNAKE_LENGTH > 1 && CUR_DIR !== 2 && CUR_DIR !== 0)) ) {
            SNAKE_MOVEMENT.unshift(0);
        } else if (k === 38 && ((CUR_DIR !== 1 && SNAKE_LENGTH === 1) || (SNAKE_LENGTH > 1 && CUR_DIR !== 3 && CUR_DIR !== 1)) ) {
            SNAKE_MOVEMENT.unshift(1);
        } else if (k === 39 && ((CUR_DIR !== 2 && SNAKE_LENGTH === 1) || (SNAKE_LENGTH > 1 && CUR_DIR !== 0 && CUR_DIR !== 2)) ) {
            SNAKE_MOVEMENT.unshift(2);
        } else if (k === 40 && ((CUR_DIR !== 3 && SNAKE_LENGTH === 1) || (SNAKE_LENGTH > 1 && CUR_DIR !== 1 && CUR_DIR !== 3)) ) {
            SNAKE_MOVEMENT.unshift(3);
        }
    };
}

/**
 * Method to return a random number in range <0; max).
 * @method randomizePos
 * @param max - maximum number that can appear in range
 * @return number random value
 */
function randomizePos(max) {
    return Math.floor(Math.random() * max) * SQUARE_WIDTH
}

/**
 * Method to create an object on the gameboard.
 * @method initObject
 * @param x - left position of square
 * @param y - top position of square
 * @param n - name of the element
 * @param c - color of the element
 * @param p - parent to which element shall be added (usually gameboard or snake's body)
 * @return jQuery created object (apple, mine, snake's head or body)
 */
function initObject(x, y, n, c, p) {
    let $obj = $("<div>")
        .attr("class", n + "-block")
        .css("width", SQUARE_WIDTH + "px")
        .css("height", SQUARE_WIDTH + "px")
        .css("background-color", c)
        .css("left", x + "px")
        .css("top", y + "px")
        .css("position", "absolute")
        .appendTo(p);

    return $obj
}

/**
 * Method to randomize direction of snake. Snake's position can be sometimes generated very close to border (let's say
 * between 1-3 squares near the border) and its direction might be set into ... well the border's direction. Player
 * might now have enough time to react and the snake will die instantly. Therefore this method checks the position of
 * snake and determines into which direction he can move. For instance, if snake lands in the upper left corner, he will
 * only be able to go down or right. If he lands somewhere in the middle of gameboard but very close to right border,
 * he won't be able to go right, but still can move up, down or left.
 * @method randomizeSnakeDirection
 * @return number random direction
 */
function randomizeSnakeDirection() {
    let $snakeHead = $(".snake-head-block").position(),
        snake_x = $snakeHead.left/SQUARE_WIDTH,
        snake_y = $snakeHead.top/SQUARE_WIDTH,
        possibleDir = [0, 1, 2, 3];

    if (snake_x < 3) { possibleDir.splice(possibleDir.indexOf(0), 1); }
    if (snake_y < 3) { possibleDir.splice(possibleDir.indexOf(1), 1); }
    if (snake_x > 16) { possibleDir.splice(possibleDir.indexOf(2), 1); }
    if (snake_y > 16) { possibleDir.splice(possibleDir.indexOf(3), 1); }

    return possibleDir[Math.floor(Math.random() * possibleDir.length)];
}

/**
 * Method to initialise content of start container. Provides basic information about the game's goals, squares.
 * @method initialiseStartContainer
 */
function initialiseStartContainer() {
    $(".container-start").css("display", "block");

    let gameInfo = "<span>The goal is to eat as many apple as possible without colliding with yourself, mine or " +
                    "borders. You can move snake with your arrows (up, down, left, right). </span>",
        snakeInfo = "<span>This you shall lead " + generateSquareInfo("green") + "</span>",
        appleInfo = "<span>This you shall eat " + generateSquareInfo("red") + "</span>",
        mineInfo = "<span>This you must omit " + generateSquareInfo("black") + "</span>",
        bodyInfo = "<span>This is your body " + generateSquareInfo("lightgreen") + "</span>",
        deadInfo = "<span>This means you are dead " + generateSquareInfo("lightgray") + "</span>";

    $("#start-info").html(gameInfo + "<br><span>" + snakeInfo + appleInfo + "</span><br>" + "<br><span>" + mineInfo +
                          bodyInfo + "</span><br><span>" + deadInfo + "</span>");
}

/**
 * Method to create a square with given color.
 * @method generateSquareInfo
 * @param c - color
 * @return string square div
 */
function generateSquareInfo(c) {
    return '<div style="width: 30px; height: 30px; background-color:' + c + '"></div>';
}

/**
 * Method to generate local highscore table. If there's a local highscore, it is retrieved from localStorage,
 * otherwise a new list is created. The list is presented on the right side of the gameboard.
 * @method generateLocalHighscore
 */
function generateLocalHighscore() {
    if (localStorage.getItem('localHighscore') === null){
        initialiseLocalHighscore()
    }

    let $localHighscoreTable = $("#local-highscore-table"),
        storageHighscore = localStorage.getItem('localHighscore'),
        localHighscore = JSON.parse(storageHighscore);

    for (var i = 0;i < JSON.parse(localHighscore).length; i++) {
        let data = JSON.parse(localHighscore)[i],
            scoreLine = document.createElement("div");
        scoreLine.setAttribute("class", "local-highscore-line");
        scoreLine.innerHTML = "<b>" + (i + 1) + ".</b> " + data.userName + " __ " + data.score;
        $localHighscoreTable.append(scoreLine);
    }
}

/**
 * Method to initialise a local highscore which will be stored locally. All userNames and scores is set to null and 0.
 * @method initialiseLocalHighscore
 */
function initialiseLocalHighscore() {
    let data = "[";

    for (var i = 0; i < HIGHSCORE_ROWS; i++) {
        data += '{"userName": "None", "score": 0},';
    }

    window.localStorage.setItem('localHighscore', JSON.stringify(data.substring(data.length - 1, 0) + "]"));
}

/**
 * Method to initialise global highscore table with HIGHSCORE_ROWS rows.
 * @method initialiseGlobalHighscore
 */
// TODO
function initialiseGlobalHighscore() {
    let $globalHighscoreTable = $("#global-score-table");

    for (var i = 0;i < HIGHSCORE_ROWS; i++) {
        let scoreLine = document.createElement("div");
        scoreLine.setAttribute("class", "global-row_score-line");
        scoreLine.innerHTML = "<b>" + (i+1) + "</b>";
        let player = document.createElement("div");
        player.content = "..............";
        let score_ = document.createElement("div");
        $globalHighscoreTable.append(scoreLine);
    }
}

/**
 * Method to start te mine generator. Every 30 seconds it launches generateMine method.
 * @method startMineGenerator
 */
function startMineGenerator() {
    MINEGENERATOR = setInterval(function() {
        generateMine()
    }, 30000);
}

/**
 * Method to loop the game. When apple is eaten, the snake's length and score increases by 1. If 5 apples are eaten,
 * game speeds up by 1.25 times. Game ends when snake gets out of the gameboard, touches the mine, or bits himself. In
 * each game interval, the snake is redrawn.
 * @method startGame
 */
function startGame() {
    if (BOARD_STATE !== 2) {
        BOARD_STATE = 1;
        redrawSnake();
        if (ateApple() === true){
            NUM_EATEN_APPLES++;
            if (NUM_EATEN_APPLES === 5) {
                NUM_EATEN_APPLES = 0;
                SNAKE_SPEED /= 1.25;
                displaySpeedMsg();
            }
            generateApple();
            generateSnakeBlock();
            increaseScore();
        }
        GAMETIMEOUR = setTimeout(function() {
            startGame()
        }, SNAKE_SPEED);
    }
}

/**
 * Method used to redraw snake head and body after each interval according to direction (current or pressed). Moves
 * head one step ahead and appends last element (tail) behind it - this happens only if snake's length > 1. The
 * GAMEBOARD_OCCUPIED gets updated after each step. In case the snake touched the border, the bomb or bit himself
 * the game ends.
 * @method redrawSnake
 */
function redrawSnake() {
    if (SNAKE_MOVEMENT.length) {
        headDir = CUR_DIR = SNAKE_MOVEMENT.pop();
    } else {
        headDir = CUR_DIR
    }

    let $snakeHead = $('.snake-head-block'),
        position = DIR_DICT[headDir],
        head_y = $snakeHead.position().top,
        head_x = $snakeHead.position().left,
        updated_head_x = $snakeHead.position().top + position[1],
        updated_head_y = $snakeHead.position().left + position[0];

    $snakeHead.css("top", updated_head_x + "px")
        .css("left", updated_head_y + "px");

    appendGameboard([updated_head_y, updated_head_x]);

    // snake has tail, so we move it behind the head
    if (SNAKE_LENGTH > 1) {
        let $snakeBody = $(".snake-body-block:not(.tail)"),
            $snake_body_last = $($snakeBody.last()),
            $snakeTail = $(".snake-body-block.tail"),
            $snake_tail_x = $snakeTail.position().left,
            $snake_tail_y = $snakeTail.position().top;

        $snakeTail.css("top", head_y + "px")
            .css("left", head_x + "px");
        if ($snakeTail.length && $snake_body_last.length) {
            $snakeTail.removeClass("tail");
            $snake_body_last.addClass("tail");
        }
        removeFromGameboard([$snake_tail_x, $snake_tail_y]);
        $snakeTail.prependTo($snakeBody.parent());
    } else {
        removeFromGameboard([head_x, head_y]);
    }

    if (outreachedBorder(updated_head_x, updated_head_y) || bitHimself() || touchedMine()) {
        gameOver()
    }
}

/**
 * Method to check if snake outreached the gameboard.
 * @method outreachedBorder
 * @param y - top snake
 * @param x - left snake
 * @return boolean value if snake crossd the gameboard
 */
function outreachedBorder(y, x) {
    return ((y < 0 || y > MAX_WIDTH) || (x < 0 || x > MAX_WIDTH))
}

/**
 * Method to check if snake bit himself. Only possible if length > 4. Checks offset of snake's head and his body if
 * they collide.
 * @method bitHimself
 * @return boolean value if he bit himself
 */
function bitHimself() {
    if (SNAKE_LENGTH > 3) {
        let $snakeHead = $('.snake-head-block').offset(),
            $snakeBody = $(".snake-body-block"),
            bitHimself = false;

        $snakeBody.each(function () {
            let $snakeBodyBlock = $(this).offset();

            if ($snakeHead.top === $snakeBodyBlock.top && $snakeHead.left === $snakeBodyBlock.left) {
                bitHimself = true
            }
        });
        return bitHimself
    }
}

/**
 * Method to check if snake touched any of the mine.
 * @method touchedMine
 * @return boolean value if he touched the mine
 */
function touchedMine() {
    let $snakeHead = $('.snake-head-block').offset(),
        $mineBlocks = $(".mine-block"),
        touchedMine = false;

    $mineBlocks.each(function () {
        let $mineBlock = $(this).offset();

        if ($snakeHead.top === $mineBlock.top && $snakeHead.left === $mineBlock.left) {
            touchedMine = true;
        }
    });
    return touchedMine;
}

/**
 * Method to end game. Clear all intervals, remove key handler, generate proper game-over info and a restart button. If
 * player's score is high enough he will be asked to provide his name in order to put him in the local hall of fame :).
 * Name will appear asap te reset button is clicked.
 * @method gameOver
 */
function gameOver() {
    BOARD_STATE = 2;
    clearInterval(MINEGENERATOR);
    clearInterval(GAMETIMEOUR);
    document.removeEventListener("keydown", handleKeyPress);

    let score = SNAKE_LENGTH - 1,
        playerPosition = getHighestPos(),
        infoMessage = generateGameOverMessage(playerPosition, score);

    $('.snake-head-block').css("background-color", "gray")
        .css("z-index", 1);
    $("#restart-info").html(infoMessage);

    $(".container-restart").css("display", "block")
        .append(
            $("<span>").append(
                $("<button>").attr("id", "btn-restart")
                    .html("Try again")
                    .on("click", function() {
                        if (playerPosition < 10 && score > 0) {
                            let $playerName = $("#player-name"),
                                name;

                            if ($playerName && $playerName.val().length > 0) {
                                name = $playerName.val()
                            } else {
                                name = "anonymous"
                            }
                            updateLocalHighscore(playerPosition, name, score);
                        }
                        resetGame()
                    })
            )
        );
}

/**
 * Method seeks if player's score is currently highest on the score board and returns its position.
 * @method getHighestPos
 * @return pos - position in highscore on which player can appear
 */
function getHighestPos() {
    let $localHighscore = $(".local-highscore-line"),
        currentScore = SNAKE_LENGTH - 1;

    for (var pos = 0; pos < HIGHSCORE_ROWS; pos++) {
        let rowContent = $localHighscore[pos],
            rowContentElements = rowContent.innerText.split(" "),
            rowScore = parseInt(rowContentElements[rowContentElements.length-1]);

        if (currentScore > rowScore && currentScore !== 0) {
            break;
        }
    }
    return pos
}

/**
 * Method to generate proper game over message based on player's score.
 * @param pos - player's position in highscore
 * @param score - player's score
 * @return string
 */
function generateGameOverMessage(pos, score) {
    let content;

    if (pos === 0 && score > 0) {
        content = "<span><b>You set a new record!</b></span><br><span>Your score is " + score + ".</span> " +
            "<span><input type='text' id='player-name' value='' maxlength='10' placeholder='Nickname...'></span>";
    } else if (pos < 10 && score > 0)  {
        content = "<span><b>You lost!</b></span><br><span>Your score is " + score + ".</span> " +
            "<span><input type='text' id='player-name' value='' maxlength='10' placeholder='Nickname'></span>";
    } else {
        content = "<span><b>You lost!</b></span><br><span>Your score is " + score + ".<br> Better luck next time.</span>";
    }

    return content
}

/**
 * Method to update local highscore board. It prepends a new highscore before all scores, which are lower or equal than
 * the current one. It also creates a new JSON containing results to be put in localstorage.
 * @param pos - position on highscore
 * @param name - player's name
 * @param score - player's score
 * @method updateLocalHighscore
 */
function updateLocalHighscore(pos, name, score) {
    let $localHighscore = $(".local-highscore-line"),
        newContent = "<b>" + (pos + 1) + ".</b> " + name + " __ " + score,
        newJSON = "[";

    for (var i = 0; i < HIGHSCORE_ROWS; i++) {
        if (i >= pos) {
            let currentContent = $localHighscore[i].innerHTML;
            $localHighscore[i].innerHTML= newContent;
            newContent = currentContent;
        }
        // if nickname contains whitespace
        let contentSplit = $localHighscore[i].innerHTML.split(" "),
            score = contentSplit[contentSplit.length - 1];
        if (contentSplit.length === 4) {
            name = contentSplit[1]
        } else {
            name = contentSplit.slice(1, contentSplit.length - 2).join();
        }
        newJSON += '{"userName": "' + name + '", "score": ' + score + '},';
    }
    window.localStorage.setItem("localHighscore", JSON.stringify(newJSON.substring(newJSON.length-1, 0) + "]"))
}

//TODO
function updateGlobalHighscore() {
}

/**
 * Method to tell if apple was eaten by snake.
 * @method ateApple
 * @return boolean value if the snake ate theapple
 */
function ateApple() {
    let $apple_pos = $(".apple-block").position(),
        $snake_head_pos = $(".snake-head-block").position();

    return ($apple_pos.top === $snake_head_pos.top) && ($apple_pos.left === $snake_head_pos.left);
}

/**
 * Method to inform user that snake increased his speed.
 * @method displaySpeedMsg
 */
function displaySpeedMsg() {
    $(".message-container").prepend(
        $("<div>").attr("class", "speed-message")
            .html("SPEED UP!")
            .css("font-size", "20px")
            .css("color", "green")
            .fadeOut('slow', function(){
                $(this).delay(8000).fadeOut();
            })
            .remove()
    );
}

/**
 * Method to generate apple on random, empty square on gameboard.
 * @method generateApple
 */
function generateApple() {
    let $apple = $(".apple-block");

    do {
        var apple_xpos = randomizePos(MAX_WIDTH_COL),
            apple_ypos = randomizePos(MAX_HEIGHT_COL);
    } while(isOnGameboard([apple_xpos, apple_ypos]));

    appendGameboard([apple_xpos, apple_ypos]);

    $apple.css("top", apple_ypos + "px")
        .css("left", apple_xpos + "px");

}

/**
 * Method to generate a mine on random, empty square on gameboard.
 * @method generateMine
 */
function generateMine() {
    let $game = $("#game");

    do {
        var mine_xpos = randomizePos(MAX_WIDTH_COL),
            mine_ypos = randomizePos(MAX_HEIGHT_COL);
    } while(isOnGameboard([mine_xpos, mine_ypos]));

    appendGameboard([mine_xpos, mine_ypos]);
    displayMineMsg();
    initObject(mine_xpos, mine_ypos, "mine", "black", $game);
}

/**
 * Method to inform user that a mine appeared on gameboard.
 * @method displayMineMsg
 */
function displayMineMsg() {
    $(".message-container").prepend(
        $("<div>").attr("class", "bomb-message")
            .html("A WILD BOMB APPEARS!")
            .css("font-size", "20px")
            .css("color", "black")
            .fadeOut('slow', function(){
                $(this).delay(8000).fadeOut();
            })
            .remove()
    );
}

/**
 * Method used to generate a new snake block and append it to tail (or head if the length of snake is equal to 1).
 * @method generateSnakeBlock
 */
// TODO prevent from creatin new position outside board.
function generateSnakeBlock() {
    let $game = $("#game"),
        $snakeHead = $(".snake-head-block"),
        $snakeTail = $(".snake-body-block.tail"),
        $snakeBody = $(".snake-body-block"),
        shift_x = shift_y = 0;

    if (SNAKE_LENGTH === 1) {
        $snakeTail = $snakeHead.position();
    } else {
        $snakeTail = $snakeTail.position();
    }

    if (CUR_DIR === 0) {
        shift_x = -SQUARE_WIDTH;
    } else if (CUR_DIR === 1) {
        shift_y = -SQUARE_WIDTH;
    } else if (CUR_DIR === 2) {
        shift_x = SQUARE_WIDTH;
    } else {
        shift_y = SQUARE_WIDTH;
    }

    SNAKE_LENGTH++;
    let updated_snake_tail_x = $snakeTail.left - shift_x,
        updated_snake_tail_y = $snakeTail.top - shift_y,
        newTail = initObject(updated_snake_tail_x, updated_snake_tail_y, "snake-body", "lightgreen", $game);
    $(newTail).addClass("tail");
    $snakeBody.removeClass("tail");

    appendGameboard([updated_snake_tail_x, updated_snake_tail_y]);
}

/**
 * Method used to increment score value on board. Since after each eaten apple the length of snake and score increases,
 * the score value is calculated based on snake length - SNAKE_LENGTH - 1 is our score.
 * @method increaseScore
 */
function increaseScore() {
    $("#score").html("Score: " + (SNAKE_LENGTH - 1))
}

/**
 * Method to reset game. Set some data back to initial values. Hide gameOver message and bring back the initial message.
 * @method resetGame
 */
function resetGame() {
    BOARD_STATE = 0;
    SNAKE_SPEED = 300;
    SNAKE_LENGTH = 1;
    CUR_DIR = 0;
    NUM_EATEN_APPLES = 0;
    GAMEBOARD_OCCUPIED = [];
    SNAKE_MOVEMENT = [];

    $(".container-restart").css("display", "none");
    $("#btn-restart").remove();
    $(".container-start").css("display", "block");
    initialiseGameData();
}